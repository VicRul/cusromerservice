FROM java:11
EXPOSE 8080
ADD /target/customerservice-1.0.jar customerservice-1.0.jar
ENTRYPOINT ["java","-jar","customerservice-1.0.jar"]