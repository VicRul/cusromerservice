package org.vicrul.customerservice.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum Sex {

    MALE("male"), FEMALE("female");

    @Getter
    private final String alias;
}
