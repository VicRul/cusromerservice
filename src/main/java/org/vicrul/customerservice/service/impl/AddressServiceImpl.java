package org.vicrul.customerservice.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.vicrul.customerservice.exception.AddressFieldException;
import org.vicrul.customerservice.model.entity.Address;
import org.vicrul.customerservice.repository.AddressRepository;
import org.vicrul.customerservice.service.AddressService;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;

    @Override
    public Address saveAddress(Address address) {
        checkAddress(address);
        return addressRepository.save(address);
    }

    @Override
    public List<Address> saveAllAddresses(Address registeredAddress, Address actualAddress) {
        List<Address> savedAddresses = new ArrayList<>();
        savedAddresses.add(saveAddress(registeredAddress));
        savedAddresses.add(saveAddress(actualAddress));
        return savedAddresses;
    }

    private void checkAddress(Address addressForChecking) {
        if (addressForChecking == null)
            throw new AddressFieldException();
    }
}
