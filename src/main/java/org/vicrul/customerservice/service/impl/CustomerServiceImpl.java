package org.vicrul.customerservice.service.impl;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;
import org.vicrul.customerservice.model.entity.Address;
import org.vicrul.customerservice.model.entity.Customer;
import org.vicrul.customerservice.exception.EmptyRequestParamsException;
import org.vicrul.customerservice.exception.SexFieldException;
import org.vicrul.customerservice.model.Sex;
import org.vicrul.customerservice.repository.CustomerRepository;

import lombok.AllArgsConstructor;
import org.vicrul.customerservice.service.AddressService;
import org.vicrul.customerservice.service.CustomerService;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final AddressService addressService;

    @Override
    public Customer saveCustomer(Customer newCustomer, boolean actualAddressIsRegisteredAddress) {
        checkSexField(newCustomer.getSex());
        Customer customerForSave = preparingToSave(newCustomer, actualAddressIsRegisteredAddress);
        return customerRepository.save(customerForSave);
    }

    @Override
    public List<Customer> findByFirstNameAndLastName(String firstName, String lastName) {
        checkCustomerInitials(firstName, lastName);
        List<Customer> foundedCustomers = customerRepository.findByFirstNameAndLastName(firstName, lastName);
        return (foundedCustomers.size() > 0) ? foundedCustomers : Collections.emptyList();
    }

    @Override
    @Transactional
    public boolean updateActualAddress(long customerId, Address newActualAddress) {
        Address newSavedAddress = addressService.saveAddress(newActualAddress);
        return customerRepository.updateActualAddress(customerId, newSavedAddress) == 1;
    }

    @Override
    public List<Customer> findAllCustomers() {
        return customerRepository.findAll();
    }

    private void checkSexField(String sexFieldValue) {
        if (sexFieldValue == null || isNotCorrectSexFieldValue(sexFieldValue))
            throw new SexFieldException();
    }

    private boolean isNotCorrectSexFieldValue(String sexFieldValue) {
        String correctMaleValue = Sex.MALE.getAlias();
        String correctFemaleValue = Sex.FEMALE.getAlias();
        if (!(sexFieldValue.equals(correctMaleValue)) && !(sexFieldValue.equals(correctFemaleValue))) {
            return true;
        }
        return false;
    }

    private Customer preparingToSave(Customer newCustomer, boolean actualAddressIsRegisteredAddress) {
        Customer preparedObject = newCustomer;
        List<Address> savedAddresses = getAddressEntities(
                newCustomer.getRegistredAddress(),
                newCustomer.getActualAddress(),
                actualAddressIsRegisteredAddress
        );
        preparedObject.setRegistredAddress(savedAddresses.get(0));
        preparedObject.setActualAddress(savedAddresses.get(1));
        return preparedObject;
    }

    private List<Address> getAddressEntities(Address registeredAddress, Address actualAddress,
                                             boolean actualAddressIsRegisteredAddress) {
        List<Address> savedAddresses = new ArrayList<>();
        if (actualAddressIsRegisteredAddress) {
            Address savedAddress = addressService.saveAddress(registeredAddress);
            savedAddresses.addAll(Arrays.asList(savedAddress, savedAddress));
        } else {
            savedAddresses = addressService.saveAllAddresses(registeredAddress, actualAddress);
        }
        return savedAddresses;
    }

    private void checkCustomerInitials(String firstName, String lastName) {
        if (firstName == null || lastName == null)
            throw new EmptyRequestParamsException();
    }


}
