package org.vicrul.customerservice.service;

import org.vicrul.customerservice.model.entity.Address;

import java.util.List;

public interface AddressService {

    Address saveAddress(Address address);
    List<Address> saveAllAddresses(Address registeredAddress, Address actualAddress);
}
